﻿using Ducksboard;
using Ducksboard.Objects;
using Microsoft.AspNet.SignalR;
using System.Linq;
using System.Web.Mvc;
using MoreLinq;
using Twilio;
using Twilio.TwiML;
using Twilio.TwiML.Mvc;

namespace Queue_Demo.Controllers
{
    public class CallController : TwilioController
    {
        readonly DucksboardClient _ducksboardClient = new DucksboardClient(Settings.DucksboardApiKey);
        readonly TwilioRestClient _twilioRestClient = new TwilioRestClient(Settings.AccountSid, Settings.AuthToken);

        public ActionResult RouteCall()
        {
            var response = new TwilioResponse();
            response.BeginGather(new {action = Url.Action("QueueCall"), numDigits = "1", timeout = "30" })
                .Say("Welcome to the Twillio Call center demo.", new { voice = "alice" })
                .Say("Your call will be placed in one of three queues, and will be answered via the browser from by the next available agent.", new { voice = "alice" })
                .Say("Press 1 to speak to the sales queue. ", new { voice = "alice" })
                .Say("Press 2 to speak to the maintenance queue. ", new {voice = "alice"})
                .Say("Press 3 to speak to the advertising queue. ", new {voice = "alice"});
            response.EndGather();
            return TwiML(response);
        }

        public ActionResult QueueCall()
        {
            var response = new TwilioResponse();

            string queueName = GetQueueName();

            EnqueueCall(response, queueName);

            IncreaseQueueGraphCount(queueName);

            return TwiML(response);
        }

        private string GetQueueName()
        {
            string queueName;
            switch (Request.Params.Get("digits"))
            {
                case "1":
                    queueName = "sales";
                    break;
                case "2":
                    queueName = "maintenance";
                    break;
                case "3":
                    queueName = "advertising";
                    break;
                default:
                    queueName = string.Empty;
                    break;
            }
            return queueName;
        }

        private void EnqueueCall(TwilioResponse response, string queueName)
        {
            response.Enqueue(queueName, new
            {
                action = "http://" + System.Web.HttpContext.Current.Request.Url.Host + "/Call/DecreaseQueueGraphCount?queue=" + queueName, //url to call when the call is dequeued
                waitUrl = Url.Action("WaitInQueue")
            });
        }

        private void IncreaseQueueGraphCount(string queueName)
        {
            var data = new Numbers { Delta = 1 };
            UpdateDucksboardQueueGraphs(queueName, data);
        }

        private void UpdateDucksboardQueueGraphs(string queueName, Numbers data)
        {
            switch (queueName)
            {
                case "sales":
                    _ducksboardClient.Update(Settings.DucksboardSalesWidgetId, data);
                    break;
                case "maintenance":
                    _ducksboardClient.Update(Settings.DucksboardMaintenanceWidgetId, data);
                    break;
                case "advertising":
                    _ducksboardClient.Update(Settings.DucksboardAdvertisingWidgetId, data);
                    break;
            }
        }

        public ActionResult WaitInQueue(string CurrentQueueSize, string QueuePosition)
        {
            var response = new TwilioResponse();

            var context = GlobalHost.ConnectionManager.GetHubContext<Hubs.QueueHub>();
            context.Clients.All.reportQueueSize(CurrentQueueSize);
            response.Say(string.Format("You are number {0} in the queue.  Please hold.", QueuePosition));
            response.Play("http://demo.brooklynhacker.com/music/ramones.mp3");

            return TwiML(response);
        }

        public ActionResult DecreaseQueueGraphCount()
        {
            var data = new Numbers { Delta = -1 };

            UpdateDucksboardQueueGraphs(Request["queue"], data);

            return TwiML(new TwilioResponse());
        }

        public ActionResult Connect()
        {
            var response = new TwilioResponse();
            response.Say("Connecting you to an agent");
            var dialAttributes = new
            {
                record = "true",
            };

            var client = new Client(Request["id"]);

            response.Dial(client, dialAttributes);

            return TwiML(response);
        }

        [HttpPost]
        public void PopCall()
        {
            var queueName = Request.Params.Get("queue");

            var queue = _twilioRestClient.ListQueues().Queues.First(q => q.FriendlyName == queueName);
            var queueMembers = _twilioRestClient.ListQueueMembers(queue.Sid).QueueMembers;

            if (queueMembers.Count > 0)
            {
                var maxWaitQueueMember = queueMembers.MaxBy(member => member.WaitTime);

                var callOptions = new CallOptions
                {
                    Url = "http://" + System.Web.HttpContext.Current.Request.Url.Host + "/Call/Connect?id=" + Request["id"],
                };

                _twilioRestClient.RedirectCall(maxWaitQueueMember.CallSid, callOptions);
            }
        }

        public ActionResult RerouteTo()
        {
            var response = new TwilioResponse();

            if (Request["branch"] == "hold")
            {
                response = SetHoldResponse();
            }
            else if (Request["branch"] == "unhold")
            {
                response = SetUnholdResponse();
            }
            else if (Request["branch"] == "goodbye")
            {
                response.Say("Goodbye");
            }
            else
            {
                response = SetOfficeResponse();
            }

            return TwiML(response);
        }

        private TwilioResponse SetUnholdResponse()
        {
            var response = new TwilioResponse();
            var client = new Client(Request["agentId"]);
            response.Dial(client);

            return response;
        }

        private TwilioResponse SetHoldResponse()
        {
            var response = new TwilioResponse();

            response.Say("You are now on hold.");
            response.Play("http://demo.brooklynhacker.com/music/ramones.mp3");

            return response;
        }

        private TwilioResponse SetOfficeResponse()
        {
            var response = new TwilioResponse();

            string loopString;
            string prompt;
            SetRerouteSayString(out loopString, out prompt);
            response.Say(prompt);

            var loopSayAttributes = new
            {
                loop = "0",
                pause = "1",
                voice = "alice",
            };

            response.Say(loopString, loopSayAttributes);

            return response;
        }

        private void SetRerouteSayString(out string loopString, out string prompt)
        {
            switch (Request["branch"])
            {
                case "accounting":
                    prompt = "You are being routed to accounting";
                    loopString = "This is accounting";
                    break;
                case "support":
                    prompt = "You are being routed to support";
                    loopString = "This is support";
                    break;
                case "mainswitch":
                    prompt = "You are being routed to the main switch";
                    loopString = "This is the main switch board";
                    break;
                default:
                    prompt = "error";
                    loopString = "This is the main switch board";
                    break;
            }
        }

        public string GetDialerCallSid()
        {
            return _twilioRestClient.GetCall(Request["clientCallSid"]).ParentCallSid;
        }
    }
}
