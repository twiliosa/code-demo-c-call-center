﻿using System;
using System.Diagnostics;
using System.Linq;
using Ducksboard;
using Ducksboard.Objects;
using FluentScheduler;
using Twilio;

namespace Queue_Demo.Controllers
{
    public class UpdateGraphsTask : ITask
    {
        public void Execute()
        {
            ReportAverageWait();
        }

        public string ReportAverageWait()
        {
            var twilioRestClient = new TwilioRestClient(Settings.AccountSid, Settings.AuthToken);

            var queuesResult = twilioRestClient.ListQueues();

            if (queuesResult.RestException == null && queuesResult.Queues != null)
            {
                UpdateQueueGraphs("sales", Settings.SalesAverageWaitWidgetId, Settings.SalesMaxWaitWidgetId, queuesResult, twilioRestClient);
                UpdateQueueGraphs("maintenance", Settings.MaintenanceAverageWaitWidgetId, Settings.MaintenanceMaxWaitWidgetId, queuesResult, twilioRestClient);
                UpdateQueueGraphs("advertising", Settings.AdvertisingAverageWaitWidgetId, Settings.AdvertisingMaxWaitWidgetId, queuesResult, twilioRestClient);
            }

            var lastFinishedCall = twilioRestClient.ListCalls(new CallListRequest { Status = "completed", }).Calls.First(call => call.Direction == "outbound-dial");

            var lastFinishedRecording = twilioRestClient.ListRecordings().Recordings.First(recording => recording.CallSid == lastFinishedCall.ParentCallSid);
            var parentCall = twilioRestClient.GetCall(lastFinishedCall.ParentCallSid);
            return lastFinishedRecording.Uri.ToString().Replace(".json", "") + "`" + parentCall.From + "`" + parentCall.To;
        }

        private static void UpdateQueueGraphs(string queueName, string averageWaitWidgetId, string maxWaitWidgetId, QueueResult queuesResult, TwilioRestClient twilioRestClient)
        {
            var ducksboardClient = new DucksboardClient(Settings.DucksboardApiKey);

            var queue = queuesResult.Queues.FirstOrDefault(q => q.FriendlyName == queueName);

            if (queue != null)
            {
                ducksboardClient.Update(averageWaitWidgetId,
                    new Numbers { Value = queue.AverageWaitTime });

                var members = twilioRestClient.ListQueueMembers(queue.Sid).QueueMembers;

                var maxWait = 0;

                var max = members.Max(member => member.WaitTime);
                if (max != null)
                    maxWait = (int)max;

                ducksboardClient.Update(maxWaitWidgetId, new Numbers { Value = maxWait });
            }
        }
    }

    public class MyRegistry : Registry
    {
        public MyRegistry()
        {
            Schedule<UpdateGraphsTask>().ToRunNow().AndEvery(2).Seconds();
        }
    }
}