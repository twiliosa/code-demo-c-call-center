﻿using System.Web.Mvc;
using Twilio;

namespace Queue_Demo.Controllers
{
    public class HomeController : Controller
    {
        private const string ApplicationSid = "AP5575d54e942ab49096958c7e66bec01a";

        public ActionResult Index(string id, string queue)
        {
            var capability = new TwilioCapability(Settings.AccountSid, Settings.AuthToken);

            capability.AllowClientIncoming(id);
            capability.AllowClientOutgoing(ApplicationSid);

            ViewBag.token = capability.GenerateToken();
            ViewBag.agentId = id;
            ViewBag.queue = queue;

            return View();
        }
    }
}
