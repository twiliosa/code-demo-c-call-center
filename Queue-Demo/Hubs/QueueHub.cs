﻿using Ducksboard;
using Ducksboard.Objects;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Diagnostics;
using System.Linq;
using Twilio;


namespace Queue_Demo.Hubs
{
    [HubName("queue")]
    public class QueueHub : Hub
    {
        public string GetRecordingUrls()
        {
            var twilioRestClient = new TwilioRestClient(Settings.AccountSid, Settings.AuthToken);

            var lastFinishedCall = twilioRestClient.ListCalls(new CallListRequest { Status = "completed", }).Calls.First(call => call.Direction == "outbound-dial");

            var lastFinishedRecording = twilioRestClient.ListRecordings().Recordings.First(recording => recording.CallSid == lastFinishedCall.ParentCallSid);
            var parentCall = twilioRestClient.GetCall(lastFinishedCall.ParentCallSid);
            return lastFinishedRecording.Uri.ToString().Replace(".json", "") + "`" + parentCall.From + "`" + parentCall.To;
        }
    }
}