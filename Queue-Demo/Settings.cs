﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Queue_Demo
{
    public class Settings
    {
        public static string AccountSid { get { return "INSERT ACCOUNT SID"; } }
        public static string AuthToken { get { return "INSERT AUTH TOKEN"; } }
        public static string DucksboardApiKey { get { return "INSERT DUCKSBOARD API KEY"; } }
        public static string DucksboardSalesWidgetId { get { return "INSERT DUCKSBOARD SALSE QUEUE SIZE WIDGET ID"; } }
        public static string DucksboardMaintenanceWidgetId { get { return "INSERT DUCKSBOARD MAINTENANCE QUEUE SIZE WIDGET ID"; } }
        public static string DucksboardAdvertisingWidgetId { get { return "INSERT DUCKSBOARD ADVERTISING QUEUE SIZE WIDGET ID"; } }
        public static string SalesAverageWaitWidgetId { get { return "etc"; } }
        public static string SalesMaxWaitWidgetId { get { return "etc"; } }
        public static string MaintenanceAverageWaitWidgetId { get { return "etc"; } }
        public static string MaintenanceMaxWaitWidgetId { get { return "etc"; } }
        public static string AdvertisingAverageWaitWidgetId { get { return "etc"; } }
        public static string AdvertisingMaxWaitWidgetId { get { return "etc"; } }
        public static string CustomerDialIn { get { return "INSERT TWILIO PHONE NUMBER";} }
    }
}
