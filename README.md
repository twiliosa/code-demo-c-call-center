# Twilio Csharp Client Call Center Demo
The Twilio Csharp Client Call Center Demo is a demo of the client capabilities for a mock call center. You can access a live version of this demo [here](http://twilio-call-center-demo.azurewebsites.net/?id=someAgent&queue=sales). This solution demonstrates a number of different functions that one building a call center in csharp may want to employ. Such functions include receiving and interacting with a call straight from the browser, transferring said calls, placing calls into queues, and recording calls. This demo also leverages the Ducksboard API with some basic call analytics and queue info. Most of the set up for this call center takes place in the Settings.cs file. See below for more detail  

### Settings File

```csharp
    namespace Queue_Demo
    {
        public class Settings
        {
            public static string AccountSid { get { return "INSERT ACCOUNT SID"; } }
            public static string AuthToken { get { return "INSERT AUTH TOKEN"; } }
            public static string DucksboardApiKey { get { return "INSERT DUCKSBOARD API KEY"; } }
            public static string DucksboardSalesWidgetId { get { return "INSERT DUCKSBOARD SALSE QUEUE SIZE WIDGET ID"; } }
            public static string DucksboardMaintenanceWidgetId { get { return "INSERT DUCKSBOARD MAINTENANCE QUEUE SIZE WIDGET ID"; } }
            public static string DucksboardAdvertisingWidgetId { get { return "INSERT DUCKSBOARD ADVERTISING QUEUE SIZE WIDGET ID"; } }
            public static string SalesAverageWaitWidgetId { get { return "etc"; } }
            public static string SalesMaxWaitWidgetId { get { return "etc"; } }
            public static string MaintenanceAverageWaitWidgetId { get { return "etc"; } }
            public static string MaintenanceMaxWaitWidgetId { get { return "etc"; } }
            public static string AdvertisingAverageWaitWidgetId { get { return "etc"; } }
            public static string AdvertisingMaxWaitWidgetId { get { return "etc"; } }
            public static string CustomerDialIn { get { return "INSERT TWILIO PHONE NUMBER";} }
        }
    }
```

### Accessing the Demo

Note, when accessing the live version of the demo, you will need to specify both an id for the agent and a queue. The id can be any id, but the queue must be one of the three valid queues, sales, maintenance, or advertising. For example, these are all valid instances of the demo:

* http://twilio-call-center-demo.azurewebsites.net/?id=agent&queue=sales
* http://twilio-call-center-demo.azurewebsites.net/?id=bond&queue=maintenance
* http://twilio-call-center-demo.azurewebsites.net/?id=007&queue=advertising

### Deploying

This demo can also be deployed on the public internet, for example, on Windows Azure:  http://www.microsoft.com/bizspark/azure/howtodeployazureapp.aspx

Note also that when deploying this demo, you will need to specify both an id and a queue.